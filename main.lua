debug = true

local createEnemyTimerMax = 0.4
local createEnemyTimer = createEnemyTimerMax
local score = 0

function love.load(arg)
   require("entities")
   ents.startup()
   ents.create("player", (love.graphics.getWidth() / 2), love.graphics.getHeight())
end

function love.update(dt)
   -- I always start with an easy way to exit the game
   if love.keyboard.isDown("escape") then
      love.event.push("quit")
   end

   createEnemyTimer = createEnemyTimer - (1 * dt)
   if createEnemyTimer < 0 then
      createEnemyTimer = createEnemyTimerMax

      -- Create an enemy
      local randomNumber = math.random(10, love.graphics.getWidth() - 10)
      ents.create("enemy", randomNumber)
   end

   ents:update(dt)
end

function love.draw()
   ents:draw()

   love.graphics.print("Score: " .. score, 16, 16, 0, 1, 1)
end

-- Collision detection taken function from
-- http://love2d.org/wiki/BoundingBox.lua Returns true if two boxes overlap,
-- false if they don't x1,y1 are the left-top coords of the first box, while
-- w1,h1 are its width and height x2,y2,w2 & h2 are the same, but for the second
-- box
function checkCollision(box1, box2)
   return box1.x < box2.x + box2.img:getWidth() and
      box2.x < box1.x + box1.img:getWidth() and
      box1.y < box2.y + box2.img:getHeight() and
      box2.y < box1.y + box1.img:getHeight()
end

function addscore()
   score = score + 1
end
