ents = {}
ents.objects = {}
local register = {}
local id = 0

function ents.startup()
   register["enemy"] = love.filesystem.load("entities/enemy.lua")
   register["player"] = love.filesystem.load("entities/player.lua")
   register["bullet"] = love.filesystem.load("entities/bullet.lua")
end

function ents.create(name, x, y)
   if not x then
      x = 0
   end

   if not y then
      y = 0
   end

   if register[name] then
      local ent = register[name]()
      ent.type = name
      ent:load(x, y)
      table.insert(ents.objects, ent)
      return ent
   else
      print("Error: Entity " .. name .. " does not exist!")
      return false
   end
end

function ents.derive(name)
   return love.filesystem.load("entities/" .. name .. ".lua")()
end

function ents:update(dt)
   for i, ent in ipairs(ents.objects) do
      if ent.update then
         ent:update(dt)
      end
   end

   for i, enemy in ipairs(ents.objects) do
      if enemy.type == "enemy" then
         for j, ent in ipairs(ents.objects) do
            if ent.type == "bullet" or ent.type == "player" then
               if checkcollision(enemy, ent) then
                  table.remove(ents.objects, i)
                  table.remove(ents.objects, j)
               end
            end
         end
      end
   end
end

function ents:draw()
   for i, ent in ipairs(ents.objects) do
      if ent.draw then
         ent:draw()
      end
   end
end

function ents.destroy(object)
   for i, ent in ipairs(ents.objects) do
      if ent == object then
         table.remove(ents.objects, i)
         return
      end
   end
end

-- Collision detection taken function from
-- http://love2d.org/wiki/BoundingBox.lua Returns true if two boxes overlap,
-- false if they don't x1,y1 are the left-top coords of the first box, while
-- w1,h1 are its width and height x2,y2,w2 & h2 are the same, but for the second
-- box
function checkcollision(box1, box2)
   return box1.x < box2.x + box2.img:getWidth() and
      box2.x < box1.x + box1.img:getWidth() and
      box1.y < box2.y + box2.img:getHeight() and
      box2.y < box1.y + box1.img:getHeight()
end
