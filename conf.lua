-- Configuration
function love.conf(t)
   t.title = "Scrolling Shooter Tutorial" -- The title of the window the game is in (string)
   t.version = "0.9.1" -- the LOVE version this game was mande for (string)
   t.window.width = 480 -- we want our game to be long and thin
   t.window.height = 800
   t.console = true
end
