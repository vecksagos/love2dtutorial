local ent = ents.derive("base")

function ent:load(x, y)
   self.img = love.graphics.newImage("assets/plane.png")
   self.x = x
   self.y = y - self.img:getHeight()
   self.speed = 150
   self.canShoot = true
   self.canShootTimerMax = 0.2
   self.canShootTimer = 0.2
   self.gunSound = love.audio.newSource("assets/gun-sound.wav", "static")
end

function ent:update(dt)
   if love.keyboard.isDown("left", "a") then
      if self.x > 0 - self.img:getWidth() / 2 then
         self.x = self.x - (self.speed * dt)
      end
   elseif love.keyboard.isDown("right", "d") then
      if self.x < (love.graphics:getWidth() - self.img:getWidth() / 2) then
         self.x = self.x + (self.speed * dt)
      end
   end

   if love.keyboard.isDown("up", "w") then
      if self.y > (love.graphics.getHeight() / 2) then
         self.y = self.y - (self.speed * dt)
      end
   elseif love.keyboard.isDown("down", "s") then
      if self. y < (love.graphics.getHeight() - 55) then
         self.y = self.y + self.speed * dt
      end
   end

   -- Time out how far apart our shots can be
   self.canShootTimer = self.canShootTimer - (1 * dt)
   if self.canShootTimer < 0 then
      self.canShoot = true
   end

   -- Create some bullets
   if love.keyboard.isDown(" ", "rctrl", "lctrl", "ctrl") and self.canShoot then
      ents.create("bullet", self.x + (self.img:getWidth() / 2), self.y - 20)
      self.gunSound:play()
      self.canShoot = false
      self.canShootTimer = self.canShootTimerMax
   end
end

function ent:draw()
   love.graphics.draw(self.img, self.x, self.y)
end

return ent
