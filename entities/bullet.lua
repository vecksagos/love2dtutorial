local ent = ents.derive("base")

function ent:load(x, y)
   self.img = love.graphics.newImage("assets/bullet.png")
   self.x = x
   self.y = y
   self.speed = 250
end

function ent:update(dt)
   self.y = self.y - (self.speed * dt)
   if self.y < 0 then -- remove selfs when they pass of the screen
      ents.destroy(self)
   end
end

function ent:draw()
   love.graphics.draw(self.img, self.x, self.y)
end

return ent
