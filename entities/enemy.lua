local ent = ents.derive("base")

function ent:load(x, y)
   self.img = love.graphics.newImage("assets/enemy.png")
   self.x = x
   self.y = - self.img:getHeight()
   self.speed = 200
end

function ent:update(dt)
   self.y = self.y + (self.speed * dt)
   if self.y > love.graphics.getHeight() + self.img:getHeight() then
      ents.destroy(self)
   end
end

function ent:draw()
   love.graphics.draw(self.img, self.x, self.y)
end

return ent
